pragma solidity ^0.4.18;

// https://github.com/OpenZeppelin/zeppelin-solidity/blob/master/contracts/math/SafeMath.sol
// rev. ddcae6254ea4d15204f4830bd2580bce03423aec (Dec 17, 2017)
/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        assert(c / a == b);
        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // assert(b > 0); // Solidity automatically throws when dividing by 0
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        assert(b <= a);
        return a - b;
    }

    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        assert(c >= a);
        return c;
    }
}

contract AppetissimoContract {
    
    // Enums
    enum State {
        NotSet, // initial value
        
        // artificial state to build the structure using multiple `builder` methods
        // because of Solidity limitations (can't use struct in public functions)
        Deployment,
        
        // mainstream
        Deployed,
        Funding,
            Funded, NotFunded,
        PrePayment,
            PrePaid, PrePaymentFailed,
        WaitingForDevelopment,
            Development, DevelopmentNotStarted,
            Implemented, NotImplemented,
        Confirmation,
            Confirmed, NotConfirmed,
        FullPayment,
            FullPaid, FullPaymentFailed,
        Success,
        
        // alternate
        Cancelled,
        Refund,
            Refunded, RefundFailed,
        Failed
    }
    
    enum PledgeType {
        NotSet, // initial value
        
        Fixed,
        Variable
    }
    
    enum PrepaymentType {
        NotSet, // initial value
        
        No,
        Fixed,
        Percent
    }
    
    enum ConfirmationType {
        NotSet, // initial value
        
        No,
        User,
        Moderator
    }
    
    // Events
    event onAdded(uint featureKey);
    event onPledgeSet(uint featureKey);
    event onPrepaymentSet(uint featureKey);
    event onConfirmationSet(uint featureKey);
    event onCancelled(uint featureKey, uint date, bool byDeveloper);
    event onDeployed(uint featureKey);
    event onFunding(uint featureKey, uint date);
    event onPledged(uint featureKey, uint date, address backer, uint256 amount);
    event onPledgeFailed(uint featureKey, uint date, address backer, uint256 amount);
    event onFunded(uint featureKey, uint date, uint256 amount);
    event onNotFunded(uint featureKey, uint date, uint256 amount);
    event onPrepayment(uint featureKey, uint date, address developer, uint256 amount);
    event onPrepaid(uint featureKey, uint date, address developer, uint256 amount);
    event onPrepaymentFailed(uint featureKey, uint date, address developer, uint256 amount);
    event onWaitingForDevelopment(uint featureKey, uint date);
    event onDevelopment(uint featureKey, uint date);
    event onDevelopmentNotStarted(uint featureKey, uint date);
    event onImplemented(uint featureKey, uint date);
    event onNotImplemented(uint featureKey, uint date);
    event onConfirmation(uint featureKey, uint date);
    event onConfirmedByUser(uint featureKey, uint date, address backer, bool positive);
    event onConfirmed(uint featureKey, uint date, bool byUsers);
    event onNotConfirmed(uint featureKey, uint date, bool byUsers);
    event onFullPayment(uint featureKey, uint date, address developer, uint256 amount);
    event onFullPaid(uint featureKey, uint date, address developer, uint256 amount);
    event onFullPaymentFailed(uint featureKey, uint date, address developer, uint256 amount);
    event onRefund(uint featureKey, uint date);
    event onPledgeRefunded(uint featureKey, uint date, address backer, uint256 amount);
    event onRefunded(uint featureKey, uint date);
    event onRefundFailed(uint featureKey, uint date);
    event onSuccess(uint featureKey, uint date);
    event onFailed(uint featureKey, uint date);

    // pledge types
    struct FixedPledge {
        uint256 amount;
    }
    
    struct VariablePledge {
        uint256 minPledge;
        uint256 maxPledge;
    }
    
    // prepayments
    struct FixedPrepayment {
        uint256 amount;
    }
    
    struct PercentPrepayment {
        uint16 percent;
    }
    
    // confirmations
    struct UserConfirmation {
        uint8 minPercent;
        uint16 duration; // days
    }
    
    struct Feature {
        uint key;
        uint256 goal;
        bool finishOnGoal;
        uint campaignStart; // timestamp
        uint campaignFinish; // timestamp
        
        PledgeType pledgeType;
        FixedPledge fixedPledge;
        VariablePledge variablePledge;
        
        PrepaymentType prepaymentType;
        FixedPrepayment fixedPrepayment;
        PercentPrepayment percentPrepayment;
        
        uint16 maxDevelopmentDelay; // days
        uint16 developmentDuration; // days
        
        ConfirmationType confirmationType;
        UserConfirmation userConfirmation;
        
        address developer;
    }
    
    struct FeatureData {
        State state;
        mapping (address => uint256) pledges;
        address[] backers;
        mapping (address => bool) backersMap;
        uint256 raised;
        mapping (address => bool) positiveConfirmationsMap;
        uint positiveConfirmationsAmount;
        mapping (address => bool) negativeConfirmationsMap;
        uint negativeConfirmationsAmount;
        
        mapping (address => bool) refundedBackersMap;
        
        // timestamp (when development have to be started by developer)
        uint developmentDeadline;
        
        // timestamp (when implementation have to be finished by developer)
        uint implementationDeadline;
        
        // timestamp (when confirmation have to be finished by users)
        uint confirmationDeadline;
    }

    mapping (uint => Feature) features;
    mapping (uint => FeatureData) data;
    
    mapping (address => bool) moderators;
    mapping (address => bool) owners;
    
    // modifiers
    
    // check the method is called by one of the owners only
    modifier ownerOnly {
        require(owners[msg.sender]);
        _;
    }
    
    // check the method is called by one of moderators only
    modifier moderatorOnly {
        require(moderators[msg.sender]);
        _;
    }
    
    // check the feature is listed
    modifier forFeature(uint featureKey) {
        require(data[featureKey].state != State.NotSet);
        _;
    }
    
    // check the feature is in expected state
    modifier withState(uint featureKey, State expectedState) {
        require(data[featureKey].state == expectedState);
        _;
    }
    
    // functions
    function AppetissimoContract() public {
        // add self as owner
        owners[msg.sender] =  true;
        
        // add self as moderator
        moderators[msg.sender] = true;
    }
    
    // functions: admin
    function addOwner(address newOwner) public ownerOnly {
        owners[newOwner] = true;
    }
    
    function removeOwner(address existingOwner) public ownerOnly {
        owners[existingOwner] = false;
    }
    
    function addModerator(address newModerator) public ownerOnly {
        moderators[newModerator] = true;
    }
    
    function removeModerator(address existingModerator) public ownerOnly {
        moderators[existingModerator] = false;
    }
    
    // functions: workflow
    
    // common feature data
    function addFeature(
        uint key,
        uint256 goal,
        bool finishOnGoal,
        uint campaignStart,  // timestamp
        uint campaignFinish, // timestamp
        uint16 maxDevelopmentDelay,
        uint16 developmentDuration,
        address developer) public
        ownerOnly
        withState(key, State.NotSet)
    {
        Feature memory feature;
        feature.key = key;
        feature.goal = goal;
        feature.finishOnGoal = finishOnGoal;
        feature.campaignStart = campaignStart;
        feature.campaignFinish = campaignFinish;
        feature.maxDevelopmentDelay = maxDevelopmentDelay;
        feature.developmentDuration = developmentDuration;
        feature.developer = developer;

        FeatureData memory featureData;
        featureData.state = State.Deployment;
        featureData.backers = new address[](0);
        featureData.raised = 0;

        features[key] = feature;
        data[key] = featureData;
    }
    
    function getFeature(uint featureKey) public
        forFeature(featureKey)
        constant returns (
            uint /* key */,
            uint256 /* goal */,
            bool /* finishOnGoal */,
            uint /* campaignStart */,
            uint /* campaignFinish */,
            uint16 /* maxDevelopmentDelay */,
            uint16 /* developmentDuration */,
            PledgeType,
            PrepaymentType,
            ConfirmationType,
            address developer)
    {
        Feature memory feature = features[featureKey];
        return (
            feature.key,
            feature.goal,
            feature.finishOnGoal,
            feature.campaignStart,
            feature.campaignFinish,
            feature.maxDevelopmentDelay,
            feature.developmentDuration,
            
            feature.pledgeType,
            feature.prepaymentType,
            feature.confirmationType,

            feature.developer
            );
    }
    
    // set fixed pledge
    function setFixedPledge(uint featureKey, uint256 amount) public
        ownerOnly
        withState(featureKey, State.Deployment)
    {
        Feature storage feature = features[featureKey];
        feature.pledgeType = PledgeType.Fixed;
        feature.fixedPledge = FixedPledge({amount: amount});

        // event
        onPledgeSet(featureKey);
    }
    
    function getFixedPledge(uint featureKey) public
        forFeature(featureKey)
        constant returns (uint256)
    {
        Feature memory feature = features[featureKey];
        require(feature.pledgeType == PledgeType.Fixed);
        return feature.fixedPledge.amount;
    }
    
    // set variable pledge
    function setVariablePledge(uint featureKey, uint256 minPledge, uint256 maxPledge) public
        ownerOnly
        withState(featureKey, State.Deployment)
    {
        Feature storage feature = features[featureKey];
        feature.pledgeType = PledgeType.Variable;
        feature.variablePledge = VariablePledge({minPledge: minPledge, maxPledge: maxPledge});

        // event
        onPledgeSet(featureKey);
    }
    
    function getVariablePledge(uint featureKey) public
        forFeature(featureKey)
        constant returns (uint256, uint256)
    {
        Feature memory feature = features[featureKey];
        require(feature.pledgeType == PledgeType.Variable);
        return (feature.variablePledge.minPledge, feature.variablePledge.maxPledge);
    }
    
    // set no prepayment
    function setNoPrepayment(uint featureKey) public
        ownerOnly
        withState(featureKey, State.Deployment)
    {
        Feature storage feature = features[featureKey];
        feature.prepaymentType = PrepaymentType.No;

        // event
        onPrepaymentSet(featureKey);
    }
    
    // set fixed prepayment
    function setFixedPrepayment(uint featureKey, uint256 amount) public
        ownerOnly
        withState(featureKey, State.Deployment)
    {
        Feature storage feature = features[featureKey];
        feature.prepaymentType = PrepaymentType.Fixed;
        feature.fixedPrepayment = FixedPrepayment({amount: amount});

        // event
        onPrepaymentSet(featureKey);
    }
    
    function getFixedPrepayment(uint featureKey) public
        forFeature(featureKey)
        constant returns (uint256)
    {
        Feature memory feature = features[featureKey];
        require(feature.prepaymentType == PrepaymentType.Fixed);
        return feature.fixedPrepayment.amount;
    }
    
    // set percent PrepaymentType
    function setPercentPrepayment(uint featureKey, uint8 percent) public
        ownerOnly
        withState(featureKey, State.Deployment)
    {
        // validation
        require(percent <= 100);

        Feature storage feature = features[featureKey];
        feature.prepaymentType = PrepaymentType.Percent;
        feature.percentPrepayment = PercentPrepayment({percent: percent});

        // event
        onPrepaymentSet(featureKey);
    }
    
    function getPercentPrepayment(uint featureKey) public
        forFeature(featureKey)
        constant returns (uint16)
    {
        Feature memory feature = features[featureKey];
        require(feature.prepaymentType == PrepaymentType.Percent);
        return feature.percentPrepayment.percent;
    }
    
    // no confirmation
    function setNoConfirmation(uint featureKey) public
        ownerOnly
        withState(featureKey, State.Deployment)
    {
        Feature storage feature = features[featureKey];
        feature.confirmationType = ConfirmationType.No;

        // event
        onConfirmationSet(featureKey);
    }
    
    // user confirmation
    function setUserConfirmation(uint featureKey, uint8 minPercent, uint16 duration) public
        ownerOnly
        withState(featureKey, State.Deployment)
    {
        // validation
        require(minPercent <= 100);

        Feature storage feature = features[featureKey];
        feature.confirmationType = ConfirmationType.User;
        feature.userConfirmation = UserConfirmation({minPercent: minPercent, duration: duration});

        // event
        onConfirmationSet(featureKey);
    }
    
    function getUserConfirmation(uint featureKey) public
        forFeature(featureKey)
        constant returns (uint8, uint16)
    {
        Feature memory feature = features[featureKey];
        require(feature.confirmationType == ConfirmationType.User);
        return (feature.userConfirmation.minPercent, feature.userConfirmation.duration);
    }
    
    // moderator confirmation 
    function setModeratorConfirmation(uint featureKey) public
        ownerOnly
        withState(featureKey, State.Deployment)
    {
        Feature storage feature = features[featureKey];
        feature.confirmationType = ConfirmationType.Moderator;

        // event
        onConfirmationSet(featureKey);
    }
    
    function hasFeature(uint featureKey) public
        constant returns (bool)
    {
        return data[featureKey].state != State.NotSet;
    }
    
    function getState(uint featureKey) public
        constant returns (State)
    {
        return data[featureKey].state;
    }
    
    // deploy (end building of feature struct)
    function deployFeature(uint featureKey) public
        ownerOnly
        withState(featureKey, State.Deployment)
    {
        // check all the fields are set
        Feature memory feature = features[featureKey];
        require(feature.pledgeType != PledgeType.NotSet);
        require(feature.prepaymentType != PrepaymentType.NotSet);
        require(feature.confirmationType != ConfirmationType.NotSet);
        
        // set new change state (Deployed)
        FeatureData storage featureData = data[featureKey];
        featureData.state = State.Deployed;
        
        // fire event
        onDeployed(featureKey);
    }

    function checkFunding(uint featureKey) public
        withState(featureKey, State.Deployed)
    {
        Feature memory feature = features[featureKey];
        require(now >= feature.campaignStart);

        // set next state (Funding)
        FeatureData storage featureData = data[featureKey];
        featureData.state = State.Funding;

        // fire event
        onFunding(featureKey, now);
    }

    function receiveFunds() public
        payable
        ownerOnly
    {

    }

    function sendFunds(address receiver, uint256 amount) public
        ownerOnly returns (bool)
    {
        require(receiver != 0x0);
        return receiver.send(amount);
    }

    function fixRefund(uint featureKey) public
        ownerOnly
        withState(featureKey, State.RefundFailed)
    {
        refund_(featureKey);
    }

    function refund_(uint featureKey) private
        forFeature(featureKey)
    {
        FeatureData storage featureData = data[featureKey];
        featureData.state = State.Refund;
        
        // event
        onRefund(featureKey, now);
        
        for (uint i = 0; i < featureData.backers.length; i++) {
            address backer = featureData.backers[i];
            uint256 amount = featureData.pledges[backer];
            
            // check if already refunded
            if (featureData.refundedBackersMap[backer]) {
                // already refunded
                continue;
            }
            
            // refunding
            if (backer.send(amount)) {
                // sent
                featureData.refundedBackersMap[backer] = true;

                // event
                onPledgeRefunded(featureKey, now, backer, amount);
            } else {
                // failed to send
                featureData.state = State.RefundFailed;

                // event
                onRefundFailed(featureKey, now);
                return;
            }
        }
        
        // event
        onRefunded(featureKey, now);
        
        // next state (end)
        featureData.state = State.Failed;
        
        // event
        onFailed(featureKey, now);
    }
    
    function setWaitingForDevelopment_(uint featureKey) private
    {
        FeatureData storage featureData = data[featureKey];
        featureData.state = State.WaitingForDevelopment;
                    
        // event
        onWaitingForDevelopment(featureKey, now);   
    }

    function fixPrepayment(uint featureKey) public
        ownerOnly
        withState(featureKey, State.PrePaymentFailed)
    {
        prePayment_(featureKey);
    }

    function prePayment_(uint featureKey) private
    {
        Feature memory feature = features[featureKey];
        FeatureData storage featureData = data[featureKey];
        
        // state
        featureData.state = State.PrePayment;
        
        uint256 amount;
        if (feature.prepaymentType == PrepaymentType.Fixed) {
            amount = feature.fixedPrepayment.amount;
        } else {
            amount = SafeMath.div(
                SafeMath.mul(
                    featureData.raised,
                    feature.percentPrepayment.percent),
                100);
        }
        
        // event
        onPrepayment(featureKey, now, feature.developer, amount);
        
        if (feature.developer.send(amount)) {
            // PrePaid
            featureData.state = State.PrePaid;
            onPrepaid(featureKey, now, feature.developer, amount);

            // WaitingForDevelopment
            setWaitingForDevelopment_(featureKey);
        } else {
            // not sent
            featureData.state = State.PrePaymentFailed;
            
            // event
            onPrepaymentFailed(featureKey, now, feature.developer, amount);
        }
    }
    
    function addDays_(uint date, uint16 days_) private
        constant returns (uint)
    {
        return date + days_ * 24 * 60 * 60; // 24 hours, 60 minutes, 60 seconds
    }
    
    function checkFunded_(
        uint featureKey,
        bool finishOnGoalAchieved,
        bool finishOnGoalNotAchieved) private
    {
        Feature memory feature = features[featureKey];
        FeatureData storage featureData = data[featureKey];
        
        if (featureData.raised >= feature.goal) {
            if (finishOnGoalAchieved) {
                featureData.state = State.Funded;
                featureData.developmentDeadline = addDays_(now, feature.maxDevelopmentDelay);
            
                // fire event
                onFunded(featureKey, now, featureData.raised);
                
                // going to next state: prepayment
                if (feature.prepaymentType != PrepaymentType.No) {
                    prePayment_(featureKey);
                } else {
                    setWaitingForDevelopment_(featureKey);
                }
            }
        } else {
            if (finishOnGoalNotAchieved) {
                featureData.state = State.NotFunded;
            
                // fire event
                onNotFunded(featureKey, now, featureData.raised);
            
                // going to next state: refund
                refund_(featureKey);
            }
        }
    }
    
    function getRaised(uint featureKey) public
        forFeature(featureKey)
        constant returns (uint256)
    {
        FeatureData memory featureData = data[featureKey];
        return featureData.raised;
    }
    
    function getBackers(uint featureKey) public
        forFeature(featureKey)
        constant returns (address[])
    {
        FeatureData memory featureData = data[featureKey];
        return featureData.backers;
    }
    
    // check if the feature is funded
    function checkFunded(uint featureKey) public
        withState(featureKey, State.Funding)
    {
        Feature memory feature = features[featureKey];
        require(now >= feature.campaignFinish);

        // check & set next state (Funded/NotFunded)
        checkFunded_(featureKey, true, true);
    }
    
    function pledge(uint featureKey) public
        payable
        withState(featureKey, State.Funding)
    {
        Feature storage feature = features[featureKey];
        uint256 pledgeAmount = msg.value;
        address backer = msg.sender;
        
        // check for fixed
        if (feature.pledgeType == PledgeType.Fixed &&
            pledgeAmount != feature.fixedPledge.amount)
            revert();
            
        // check for variable
        if (feature.pledgeType == PledgeType.Variable &&
            (pledgeAmount < feature.variablePledge.minPledge
            ||
            pledgeAmount > feature.variablePledge.maxPledge))
            revert();
            
        FeatureData storage featureData = data[featureKey];
        
        // exclude double pledging by the same backer
        require(!featureData.backersMap[backer]);
        
        featureData.pledges[backer] = pledgeAmount;
        featureData.backersMap[backer] = true;
        featureData.backers.push(backer);
        featureData.raised = SafeMath.add(
            featureData.raised,
            pledgeAmount);

        // event
        onPledged(featureKey, now, backer, pledgeAmount);

        // check & set next state (Funded/NotFunded)
        checkFunded_(featureKey, feature.finishOnGoal, false);
    }
    
    function startDevelopment(uint featureKey) public
        withState(featureKey, State.WaitingForDevelopment)
    {
        Feature memory feature = features[featureKey];
        address invoker = msg.sender;
        
        // can be invoked by owner or developer
        if (!owners[invoker]) {
            require(invoker == feature.developer);
        }
        
        FeatureData storage featureData = data[featureKey];
        featureData.state = State.Development;
        featureData.implementationDeadline = addDays_(now, feature.developmentDuration);
        
        // event
        onDevelopment(featureKey, now);
    }
    
    // check if development was started after feature funded
    function checkDevelopment(uint featureKey) public
        forFeature(featureKey)
    {
        FeatureData storage featureData = data[featureKey];
        
        require(
            (featureData.state == State.WaitingForDevelopment)
            &&
            (now >= featureData.developmentDeadline)); // deadline is calculated when funded
        
        featureData.state = State.DevelopmentNotStarted;
        
        // event
        onDevelopmentNotStarted(featureKey, now);
        
        // next state
        refund_(featureKey);
    }

    function fixFullPayment(uint featureKey) public
        ownerOnly
        withState(featureKey, State.FullPaymentFailed)
    {
        fullPayment_(featureKey);
    }

    function fullPayment_(uint featureKey) private
    {
        Feature memory feature = features[featureKey];
        FeatureData storage featureData = data[featureKey];

        require(
                (featureData.state == State.FullPaymentFailed)
                 ||
                (feature.confirmationType == ConfirmationType.No
                 &&
                 featureData.state == State.Implemented)
                 ||
                (feature.confirmationType != ConfirmationType.No
                 &&
                 featureData.state == State.Confirmed)
                );
                
        uint256 amount;
        if (feature.prepaymentType == PrepaymentType.No) {
            amount = featureData.raised;
        } else if (feature.prepaymentType == PrepaymentType.Fixed) {
            amount = SafeMath.sub(
                featureData.raised,
                feature.fixedPrepayment.amount);
        } else {
            amount = SafeMath.div(
                SafeMath.mul(
                    featureData.raised,
                    SafeMath.sub(
                        100,
                        feature.percentPrepayment.percent)),
                100);
        }
        
        // event
        onFullPayment(featureKey, now, feature.developer, amount);

        // send remaining amount
        if (amount == 0 || feature.developer.send(amount)) {
            featureData.state = State.FullPaid;
            onFullPaid(featureKey, now, feature.developer, amount);
        } else {
            featureData.state = State.FullPaymentFailed;
            onFullPaymentFailed(featureKey, now, feature.developer, amount);
            return;
        }

        // end state
        featureData.state = State.Success;
        onSuccess(featureKey, now);
    }

    function cancel(uint featureKey) public
        forFeature(featureKey)
    {
        Feature memory feature = features[featureKey];
        address invoker = msg.sender;

        // can be invoked by owner or developer
        if (!owners[invoker]) {
            require(invoker == feature.developer);
        }

        FeatureData storage featureData = data[featureKey];

        require(
            featureData.state == State.Funding ||
            featureData.state == State.WaitingForDevelopment ||
            featureData.state == State.Development ||
            featureData.state == State.Confirmation);

        featureData.state = State.Cancelled;

        // event
        bool byDeveloper = (invoker == feature.developer);
        onCancelled(featureKey, now, byDeveloper);

        // next states
        refund_(featureKey);
    }

    function finishDevelopment(uint featureKey) public
        withState(featureKey, State.Development)
    {
        Feature memory feature = features[featureKey];
        address invoker = msg.sender;
        
        // can be invoked by owner or developer
        if (!owners[invoker]) {
            require(invoker == feature.developer);
        }
        
        FeatureData storage featureData = data[featureKey];
        featureData.state = State.Implemented;
        
        // have to be confirmed by users
        if (feature.confirmationType == ConfirmationType.User) {
            featureData.confirmationDeadline =
                addDays_(now, feature.userConfirmation.duration);
                
            featureData.positiveConfirmationsAmount = 0;
            featureData.negativeConfirmationsAmount = 0;
        }
        
        // event
        onImplemented(featureKey, now);
        
        // no confirmation at all
        if (feature.confirmationType == ConfirmationType.No) {
            fullPayment_(featureKey);
        } else {
            // by Users or Moderator
            featureData.state = State.Confirmation;

            // event
            onConfirmation(featureKey, now);
        }
    }
    
    // check if the feature was implemented
    function checkImplemented(uint featureKey) public
    {
        FeatureData storage featureData = data[featureKey];
        
        require(
            (featureData.state == State.Development)
            &&
            (now >= featureData.implementationDeadline)); // deadline is calculated when startDevelopment() invoked
            
        featureData.state = State.NotImplemented;
        
        // event
        onNotImplemented(featureKey, now);
        
        // next state
        refund_(featureKey);
    }

    // feature confirmation by User
    function confirmByUser(uint featureKey, address backer, bool positive) public
        withState(featureKey, State.Confirmation)
    {
        address invoker = msg.sender;
        FeatureData storage featureData = data[featureKey];
        
        // owner or backer only can invoke!
        if (!owners[invoker]) {
            require(invoker == backer && featureData.backersMap[backer]);
        }
        
        // exclude double confirmation
        require(!featureData.positiveConfirmationsMap[backer]);
        require(!featureData.negativeConfirmationsMap[backer]);
        
        if (positive) {
            featureData.positiveConfirmationsMap[backer] = true;
            featureData.positiveConfirmationsAmount += 1;
        } else {
            featureData.negativeConfirmationsMap[backer] = true;
            featureData.negativeConfirmationsAmount += 1;
        }

        // event
        onConfirmedByUser(featureKey, now, backer, positive);
    }
    
    function confirmed_(uint featureKey, bool isConfirmed, bool byUsers) private
        withState(featureKey, State.Confirmation)
    {
        // apply new state state
        FeatureData storage featureData = data[featureKey];
        
        if (isConfirmed) {
            featureData.state = State.Confirmed;
            
            // event
            onConfirmed(featureKey, now, byUsers);
            
            // next
            fullPayment_(featureKey);
        } else {
            featureData.state = State.NotConfirmed;
            
            // event
            onNotConfirmed(featureKey, now, byUsers);
            
            // next
            refund_(featureKey);
        }
    }
    
    // feature confirmation by Moderator
    function confirmByModerator(uint featureKey, bool positive) public
        moderatorOnly
        withState(featureKey, State.Confirmation)
    {
        Feature memory feature = features[featureKey];
        require(feature.confirmationType == ConfirmationType.Moderator);
        
        // apply new state
        confirmed_(featureKey, positive, false);
    }
    
    // check if the feature was confirmed
    function checkConfirmed(uint featureKey) public
        forFeature(featureKey)
        withState(featureKey, State.Confirmation)
    {
        Feature memory feature = features[featureKey];
        FeatureData storage featureData = data[featureKey];
        
        require(
            (feature.confirmationType == ConfirmationType.User)
            &&
            (now >= featureData.confirmationDeadline));
            // deadline is calculated when finishDevelopment() invoked
            
        // check
        uint requiredAmount =
            SafeMath.div(
                SafeMath.mul(
                    featureData.backers.length,
                    feature.userConfirmation.minPercent),
                100);
        
        uint notVotedBackers =
            featureData.backers.length
            - featureData.positiveConfirmationsAmount
            - featureData.negativeConfirmationsAmount;

        // assume that not voted backers are 'positive'
        uint totalPositiveConfirmationsAmount =
            featureData.positiveConfirmationsAmount + notVotedBackers;

        bool isConfirmed = (totalPositiveConfirmationsAmount >= requiredAmount);
        
        // next state
        confirmed_(featureKey, isConfirmed, true);
    }
}